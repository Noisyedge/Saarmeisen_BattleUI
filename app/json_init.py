from app.json_brain import decode_brain
from app.json_field import decode_field


def decode_init(init):
    if "width" in init and "height" in init and "brains" in init and "fields" in init:
        return Init(init["width"], init["height"], init["brains"], init["fields"])
    raise ValueError("json not correct")


class Init:
    def __init__(self, width, height, brains, fields):
        self.width = width
        self.height = height
        self.brains = []
        for brain in brains:
            self.brains.append(decode_brain(brain))
        self.fields = []
        for field in fields:
            self.fields.append(decode_field(field))
