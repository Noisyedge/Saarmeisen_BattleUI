import argparse
import ctypes
import json
import os
import sys

import pygame
import pygame.locals
from PIL import ImageFont, ImageDraw, Image

from app.ant_sprite_loader_pil import AntSprites
from app.enemy_sprite_loader_pil import EnemySprites
from app.food_sprite_loader_pil import FoodSprites
from app.json_data import GameData
from app.json_step import Step
from app.tile_sprite_loader_pil import TileSprites


def render_tile(tile, target_surface, tileclass, foodclass, enemyclass, antclass, zoomfactor, render_x, render_y):
    x = tile.x
    y = tile.y
    if tile.field_type == '.':

        tile_image = tileclass.Terrain.Grass.grass_4.copy()

        tilewidth, tileheight = tile_image.size

    elif tile.field_type == '#':
        tile_image = tileclass.Terrain.Grass.grass_14.copy()
        tilewidth, tileheight = tile_image.size
    elif tile.field_type == '=':

        tile_image = tileclass.Terrain.Grass.grass_4.copy()
        tilewidth, tileheight = tile_image.size

    else:

        tile_image = tileclass.Terrain.Grass.grass_2.copy()
        tilewidth, tileheight = tile_image.size

        path_to_font = os.path.join(os.path.dirname(__file__), "res/Fonts/antpile.ttf")
        font = ImageFont.truetype(path_to_font, int(tileheight / 2))
        d = ImageDraw.Draw(tile_image)
        w, h = font.getsize(tile.field_type)
        d.text((int((tilewidth - w) / 2), int((tileheight - h) / 2)), tile.field_type, font=font,
               fill=(255, 255, 255, 255))

    tile_image = tile_image.resize((int(tilewidth * zoomfactor), int(tileheight * zoomfactor)))

    if tile.field_type == '=':
        enemy_image = enemyclass.antlion.copy()

        ew, eh = enemy_image.size
        if ew > eh:
            scalefactor = tilewidth / 1.2 * zoomfactor
        else:
            scalefactor = tileheight / 1.2 * zoomfactor

        enemy_image = enemy_image.resize((int(scalefactor), int(scalefactor)))
        tile_image.paste(enemy_image,
                         (int((tilewidth * zoomfactor - scalefactor) / 2),
                          int((tileheight * zoomfactor - scalefactor) / 2)),
                         enemy_image)

    if tile.food is not None and tile.food > 0:
        food_image = foodclass.red_mushroom.copy()
        fw, fh = food_image.size
        if fw > fh:
            scalefactor = tilewidth / 5 / fw
        else:
            scalefactor = tileheight / 5 / fh

        path_to_font = os.path.join(os.path.dirname(__file__), "res/Fonts/opensans_bold.ttf")
        font = ImageFont.truetype(path_to_font, int(fh / 2))
        d = ImageDraw.Draw(food_image)
        w, h = font.getsize(str(tile.food))
        d.text((int((fw - w) / 2), int(fh - 2 * h) / 2), str(tile.food), font=font, fill=(0, 0, 0, 255))

        food_image = food_image.resize((int(fw * scalefactor * zoomfactor), int(fh * scalefactor * zoomfactor)))

        tile_image.paste(food_image,
                         (int((tilewidth - fw * scalefactor) / 2 * zoomfactor),
                          int((tileheight - fh * scalefactor) / 2 * zoomfactor)),
                         food_image)

    if tile.ant is not None:
        render_ant(tile.ant, antclass, tilewidth, tileheight, tile_image, zoomfactor, foodclass)

    target_surface.paste(tile_image,
                         (int((x * tilewidth + (y % 2 * tilewidth / 2)) * zoomfactor + render_x),
                          int((y * tileheight - (y * tileheight / 4)) * zoomfactor + render_y)), tile_image)


def render_ant(ant, antclass, tilewidth, tileheight, target_image, zoomfactor, foodclass):
    if "west" in ant.direction:
        if ant.swarm_id == "A":
            img = antclass.playerleft.copy()
        else:
            img = antclass.normalleft.copy()
    else:
        if ant.swarm_id == "A":
            img = antclass.playerright.copy()
        else:
            img = antclass.normalright.copy()

    aw, ah = img.size
    scale = min(tilewidth / 1.3 / aw, tileheight / 1.3 / ah)
    antwidth = aw * scale * zoomfactor
    antheight = ah * scale * zoomfactor

    if ant.carries_food:
        food_image = foodclass.red_mushroom.copy()
        fw, fh = food_image.size
        if fw > fh:
            scalefactor = aw / 4
        else:
            scalefactor = ah / 4
        food_image = food_image.resize((int(scalefactor), int(scalefactor)))

        img.paste(food_image, (int((aw - scalefactor) / 2), int((ah - scalefactor) / 2.5)), food_image)

    if ant.direction == "northwest":
        ant_image_rotated = img.rotate(-60, expand=False)
        ant_image_rotated = ant_image_rotated.resize((int(antwidth), int(antheight)))

    elif ant.direction == "northeast":
        ant_image_rotated = img.rotate(60, expand=False)
        ant_image_rotated = ant_image_rotated.resize((int(antwidth), int(antheight)))

    elif ant.direction == "east":
        ant_image_rotated = img
        ant_image_rotated = ant_image_rotated.resize((int(antwidth), int(antheight)))
    elif ant.direction == "southeast":
        ant_image_rotated = img.rotate(-60, expand=False)
        ant_image_rotated = ant_image_rotated.resize((int(antwidth), int(antheight)))

    elif ant.direction == "southwest":
        ant_image_rotated = img.rotate(60, expand=False)
        ant_image_rotated = ant_image_rotated.resize((int(antwidth), int(antheight)))
    else:
        ant_image_rotated = img
        ant_image_rotated = ant_image_rotated.resize((int(antwidth), int(antheight)))

    offset = (
        int((tilewidth * zoomfactor - antwidth) / 2), int((tileheight * zoomfactor - antheight) / 2))

    target_image.paste(ant_image_rotated, offset, ant_image_rotated)


def start_game(game_data):
    pygame.init()
    if "win32" == sys.platform or "cygwin" == sys.platform:
        ctypes.windll.user32.SetProcessDPIAware()
    clock = pygame.time.Clock()
    fps = 60
    width = game_data.init.width
    height = game_data.init.height
    screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN | pygame.HWSURFACE | pygame.DOUBLEBUF)
    w, h = screen.get_size()
    diry = os.path.join(os.path.dirname(__file__), "res")
    tileclass = TileSprites(diry)
    antclass = AntSprites(diry)
    foodclass = FoodSprites(diry)
    enemyclass = EnemySprites(diry)
    width_of_tile, height_of_tile = (120, 140)
    tile_map = Image.new("RGB", (w, h))

    step = Step([], [])
    step.standings = game_data.steps[0].standings
    step.fields = game_data.init.fields
    turns = [step]
    turns.extend(game_data.steps)
    current_turn = 0
    startzoom = min(w / (width_of_tile * (width + 0.5)),
                    h / (height * height_of_tile - ((height - 1) * height_of_tile / 4)))
    zoomfactor = startzoom

    dirty_zoom = True
    dirty_map = True
    render_x, render_y = (0, 0)
    turns_tiles = [turns[0].fields]
    for turnnum in range(0, len(game_data.steps)):
        turns_tiles.append(turns_tiles[turnnum].copy())
        for tile in turns[turnnum + 1].fields:
            turns_tiles[turnnum + 1][tile.y * width + tile.x] = tile
    while True:
        clock.tick(fps)
        if dirty_map or dirty_zoom:
            screen.fill(pygame.Color("black"))
            tile_map_img = tile_map.copy()
            for tile in turns_tiles[current_turn]:

                left = ((tile.x + 1) * width_of_tile + (tile.y % 2 * width_of_tile / 2)) * zoomfactor + render_x
                right = left - (width_of_tile * zoomfactor)
                up = ((tile.y + 1) * height_of_tile - (tile.y * height_of_tile / 4)) * zoomfactor + render_y
                down = up - (height_of_tile * zoomfactor) + render_y
                if left > 0 and right < w and up > 0 and down < h:
                    render_tile(tile, tile_map_img, tileclass, foodclass, enemyclass, antclass, zoomfactor, render_x,
                                render_y)
            dirty_map = False
            dirty_zoom = False
            surf = pygame.image.fromstring(tile_map_img.tobytes(), tile_map_img.size, tile_map_img.mode)
            screen.blit(surf, (0, 0))

            pygame.display.flip()
        keys = pygame.key.get_pressed()
        if keys[pygame.locals.K_w]:
            render_y += 80
            if render_y > 0:
                render_y = 0
            dirty_map = True
        if keys[pygame.locals.K_a]:
            render_x += 80
            if render_x > 0:
                render_x = 0
            dirty_map = True
        if keys[pygame.locals.K_d]:
            render_x -= 80
            maxrendx = -((width + 0.5) * width_of_tile * zoomfactor - w)
            if render_x < maxrendx:
                render_x = min(maxrendx, 0)
            dirty_map = True
        if keys[pygame.locals.K_s]:
            render_y -= 80
            maxrendy = -((height_of_tile * height - ((height - 1) * height_of_tile / 4)) * zoomfactor - h)
            if render_y < maxrendy:
                render_y = min(maxrendy, 0)
            dirty_map = True
        if keys[pygame.locals.K_e]:
            zoomfactor += 0.1
            dirty_zoom = True
        if keys[pygame.locals.K_q]:
            zoomfactor -= 0.1
            if zoomfactor < startzoom:
                zoomfactor = startzoom
            maxrendx = -((width + 0.5) * width_of_tile * zoomfactor - w)
            if render_x < maxrendx:
                render_x = min(maxrendx, 0)
            maxrendy = -((height_of_tile * height - ((height - 1) * height_of_tile / 4)) * zoomfactor - h)
            if render_y < maxrendy:
                render_y = min(maxrendy, 0)
            dirty_zoom = True
        step_width = 1
        if keys[pygame.locals.K_LSHIFT]:

            if keys[pygame.locals.K_LEFT]:
                current_turn -= step_width
                dirty_map = True
                current_turn = max(0, current_turn)

            if keys[pygame.locals.K_RIGHT]:
                current_turn += step_width
                dirty_map = True
                current_turn = min(current_turn, len(turns) - 1)

        for event in pygame.event.get():
            if event.type == pygame.locals.KEYDOWN:
                if event.key == pygame.locals.K_ESCAPE:
                    pygame.quit()
                    sys.exit(0)
                if event.key == pygame.locals.K_LEFT and not keys[pygame.locals.K_LSHIFT]:
                    current_turn -= step_width
                    dirty_map = True
                    current_turn = max(0, current_turn)
                if event.key == pygame.locals.K_RIGHT and not keys[pygame.locals.K_LSHIFT]:
                    current_turn += step_width
                    dirty_map = True
                    current_turn = min(current_turn, len(turns) - 1)
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit(0)


def main():
    parser = argparse.ArgumentParser(description="GUI for Sopra2018 Saants")
    parser.add_argument("input")
    args = parser.parse_args()
    inputfile = args.input
    try:
        file = open(inputfile, "r")
    except Exception as e:
        print(e)
        sys.exit(2)
    jsonstring = file.read()
    json_obj = json.loads(jsonstring)
    game_data = GameData(json_obj)
    start_game(game_data)


if __name__ == "__main__":
    main()
