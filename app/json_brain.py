def decode_brain(brain):
    if 'name' in brain and 'swarm_id' in brain and 'instructions' in brain:
        return Brain(brain['name'], brain["swarm_id"], brain['instructions'])

    raise ValueError("Wrong Json")

class Brain:
    def __init__(self, name, swarm_id, instructions):
        self.name = name
        self.swarm_id = swarm_id
        self.instructions = instructions
