import os
import random
from enum import Enum
from PIL import Image


class TileTypes(Enum):
    Medieval = 0
    Military = 1
    Mill = 2
    Modern = 3
    Scifi = 4
    Western = 5
    Dirt = 6
    Grass = 7
    Mars = 8
    Sand = 9
    Stone = 9


class TileSprites:
    def __init__(self, path):
        path = os.path.join(path, "Tiles")
        self.Medieval = Medieval(path)
        self.Military = Military(path)
        self.Mill = Mill(path)
        self.Modern = Modern(path)
        self.Scifi = Scifi(path)
        self.Terrain = Terrain(path)
        self.Western = Western(path)

    def get_random_tile(self):
        rnd = random.randint(1, 10)
        if rnd > 5:
            rnd = random.randint(1, 5)
            return self.Terrain.get_random_tile(rnd)
        else:
            rnd = random.randint(1, 6)
            if rnd == 1:
                return self.Medieval.get_random_tile()
            if rnd == 2:
                return self.Military.get_random_tile()
            if rnd == 3:
                return self.Mill.get_random_tile()
            if rnd == 4:
                return self.Modern.get_random_tile()
            if rnd == 5:
                return self.Scifi.get_random_tile()
            if rnd == 6:
                return self.Western.get_random_tile()

    def get_random_tile_of_type(self, tiletype):
        if tiletype == TileTypes.Western:
            return self.Western.get_random_tile()
        if tiletype == TileTypes.Scifi:
            return self.Scifi.get_random_tile()
        if tiletype == TileTypes.Modern:
            return self.Modern.get_random_tile()
        if tiletype == TileTypes.Mill:
            return self.Mill.get_random_tile()
        if tiletype == TileTypes.Military:
            return self.Military.get_random_tile()
        if tiletype == TileTypes.Medieval:
            return self.Medieval.get_random_tile()
        if tiletype == TileTypes.Sand:
            return self.Terrain.Sand.get_random_tile()
        if tiletype == TileTypes.Stone:
            return self.Terrain.Stone.get_random_tile()
        if tiletype == TileTypes.Mars:
            return self.Terrain.Mars.get_random_tile()
        if tiletype == TileTypes.Grass:
            return self.Terrain.Grass.get_random_tile()
        if tiletype == TileTypes.Dirt:
            return self.Terrain.Dirt.get_random_tile()


class Medieval:
    def __init__(self, path):
        self.archery = Image.open(os.path.join(path, "Medieval/medieval_archery.png"))
        self.archway = Image.open(os.path.join(path, "Medieval/medieval_archway.png"))
        self.blacksmith = Image.open(os.path.join(path, "Medieval/medieval_blacksmith.png"))
        self.cabin = Image.open(os.path.join(path, "Medieval/medieval_cabin.png"))
        self.church = Image.open(os.path.join(path, "Medieval/medieval_church.png"))
        self.farm = Image.open(os.path.join(path, "Medieval/medieval_farm.png"))
        self.house = Image.open(os.path.join(path, "Medieval/medieval_house.png"))
        self.largeCastle = Image.open(os.path.join(path, "Medieval/medieval_largeCastle.png"))
        self.lumber = Image.open(os.path.join(path, "Medieval/medieval_lumber.png"))
        self.mine = Image.open(os.path.join(path, "Medieval/medieval_mine.png"))
        self.openCastle = Image.open(os.path.join(path, "Medieval/medieval_openCastle.png"))
        self.ruins = Image.open(os.path.join(path, "Medieval/medieval_ruins.png"))
        self.smallCastle = Image.open(os.path.join(path, "Medieval/medieval_smallCastle.png"))
        self.tower = Image.open(os.path.join(path, "Medieval/medieval_tower.png"))
        self.windmill = Image.open(os.path.join(path, "Medieval/medieval_windmill.png"))

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 14))

    def num_to_tile(self, num):
        if num == 1:
            return self.archery
        if num == 2:
            return self.archway
        if num == 3:
            return self.blacksmith
        if num == 4:
            return self.cabin
        if num == 5:
            return self.church
        if num == 6:
            return self.farm
        if num == 15:
            return self.house

        if num == 7:
            return self.largeCastle

        if num == 8:
            return self.lumber

        if num == 9:
            return self.mine

        if num == 10:
            return self.openCastle

        if num == 11:
            return self.ruins
        if num == 12:
            return self.smallCastle
        if num == 13:
            return self.tower
        if num == 14:
            return self.windmill


class Military:
    def __init__(self, path):
        self.entrance = Image.open(os.path.join(path, "Military/military_entrance.png"))
        self.hangar = Image.open(os.path.join(path, "Military/military_hangar.png"))
        self.rockets = Image.open(os.path.join(path, "Military/military_rockets.png"))
        self.tanks = Image.open(os.path.join(path, "Military/military_tanks.png"))
        self.turretLarge = Image.open(os.path.join(path, "Military/military_turretLarge.png"))
        self.turretMedium = Image.open(
            os.path.join(path, "Military/military_turretMedium.png"))
        self.turretSmall = Image.open(os.path.join(path, "Military/military_turretSmall.png"))

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 7))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.entrance
        if rnd == 2:
            return self.hangar
        if rnd == 3:
            return self.rockets
        if rnd == 4:
            return self.tanks
        if rnd == 5:
            return self.turretLarge
        if rnd == 6:
            return self.turretMedium
        if rnd == 7:
            return self.turretSmall


class Mill:
    def __init__(self, path):
        self.crane = Image.open(os.path.join(path, "Mill/mill_crane.png"))
        self.cutter = Image.open(os.path.join(path, "Mill/mill_cutter.png"))
        self.factory = Image.open(os.path.join(path, "Mill/mill_factory.png"))
        self.stoneWarehouse = Image.open(os.path.join(path, "Mill/mill_stoneWarehouse.png"))
        self.storage = Image.open(os.path.join(path, "Mill/mill_storage.png"))
        self.warehouse = Image.open(os.path.join(path, "Mill/mill_warehouse.png"))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.crane
        if rnd == 2:
            return self.cutter
        if rnd == 3:
            return self.factory
        if rnd == 4:
            return self.stoneWarehouse
        if rnd == 5:
            return self.storage
        if rnd == 6:
            return self.warehouse

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 6))


class Modern:
    def __init__(self, path):
        self.campsite = Image.open(os.path.join(path, "Modern/modern_campsite.png"))
        self.cornerShop = Image.open(os.path.join(path, "Modern/modern_cornerShop.png"))
        self.house = Image.open(os.path.join(path, "Modern/modern_house.png"))
        self.houseSmall = Image.open(os.path.join(path, "Modern/modern_houseSmall.png"))
        self.largeBuilding = Image.open(os.path.join(path, "Modern/modern_largeBuilding.png"))
        self.oldBuilding = Image.open(os.path.join(path, "Modern/modern_oldBuilding.png"))
        self.petrol = Image.open(os.path.join(path, "Modern/modern_petrol.png"))
        self.shop = Image.open(os.path.join(path, "Modern/modern_shop.png"))
        self.skyscraper = Image.open(os.path.join(path, "Modern/modern_skyscraper.png"))
        self.skyscraperGlass = Image.open(
            os.path.join(path, "Modern/modern_skyscraperGlass.png"))
        self.trailerpark = Image.open(os.path.join(path, "Modern/modern_trailerpark.png"))
        self.villa = Image.open(os.path.join(path, "Modern/modern_villa.png"))
        self.villageLarge = Image.open(os.path.join(path, "Modern/modern_villageLarge.png"))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.campsite
        if rnd == 2:
            return self.cornerShop
        if rnd == 3:
            return self.house
        if rnd == 4:
            return self.houseSmall
        if rnd == 5:
            return self.largeBuilding
        if rnd == 6:
            return self.oldBuilding
        if rnd == 7:
            return self.petrol
        if rnd == 8:
            return self.shop
        if rnd == 9:
            return self.skyscraper
        if rnd == 10:
            return self.skyscraperGlass
        if rnd == 11:
            return self.trailerpark
        if rnd == 12:
            return self.villa
        if rnd == 13:
            return self.villageLarge

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 13))


class Scifi:
    def __init__(self, path):
        self.base = Image.open(os.path.join(path, "Sci-fi/scifi_base.png"))
        self.building = Image.open(os.path.join(path, "Sci-fi/scifi_building.png"))
        self.cargo = Image.open(os.path.join(path, "Sci-fi/scifi_cargo.png"))
        self.corner = Image.open(os.path.join(path, "Sci-fi/scifi_corner.png"))
        self.domes = Image.open(os.path.join(path, "Sci-fi/scifi_domes.png"))
        self.energy = Image.open(os.path.join(path, "Sci-fi/scifi_energy.png"))
        self.factory = Image.open(os.path.join(path, "Sci-fi/scifi_factory.png"))
        self.factoryHangar = Image.open(os.path.join(path, "Sci-fi/scifi_factoryHangar.png"))
        self.factoryHigh = Image.open(os.path.join(path, "Sci-fi/scifi_factoryHigh.png"))
        self.foliage = Image.open(os.path.join(path, "Sci-fi/scifi_foliage.png"))
        self.hangar = Image.open(os.path.join(path, "Sci-fi/scifi_hangar.png"))
        self.headquarters = Image.open(os.path.join(path, "Sci-fi/scifi_headquarters.png"))
        self.living = Image.open(os.path.join(path, "Sci-fi/scifi_living.png"))
        self.port = Image.open(os.path.join(path, "Sci-fi/scifi_port.png"))
        self.silo = Image.open(os.path.join(path, "Sci-fi/scifi_silo.png"))
        self.skyscraper = Image.open(os.path.join(path, "Sci-fi/scifi_skyscraper.png"))
        self.tower = Image.open(os.path.join(path, "Sci-fi/scifi_tower.png"))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.base
        if rnd == 2:
            return self.building
        if rnd == 3:
            return self.cargo
        if rnd == 4:
            return self.corner
        if rnd == 5:
            return self.domes
        if rnd == 6:
            return self.energy
        if rnd == 7:
            return self.factory
        if rnd == 8:
            return self.factoryHangar
        if rnd == 9:
            return self.factoryHigh
        if rnd == 10:
            return self.foliage
        if rnd == 11:
            return self.hangar
        if rnd == 12:
            return self.headquarters
        if rnd == 13:
            return self.living
        if rnd == 14:
            return self.port
        if rnd == 15:
            return self.silo
        if rnd == 16:
            return self.skyscraper
        if rnd == 17:
            return self.tower

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 17))


class Terrain:
    def __init__(self, path):
        self.Dirt = Dirt(path)
        self.Grass = Grass(path)
        self.Mars = Mars(path)
        self.Sand = Sand(path)
        self.Stone = Stone(path)

    def get_random_tile(self, rnd):
        return self.num_to_terrain(rnd).get_random_tile()

    def num_to_terrain(self, rnd):
        if rnd == 1:
            return self.Dirt
        if rnd == 2:
            return self.Grass
        if rnd == 3:
            return self.Mars
        if rnd == 4:
            return self.Sand
        if rnd == 5:
            return self.Stone


class Dirt:
    def __init__(self, path):
        self.dirt_1 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_01.png"))
        self.dirt_2 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_02.png"))
        self.dirt_3 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_03.png"))
        self.dirt_4 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_04.png"))
        self.dirt_5 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_05.png"))
        self.dirt_6 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_06.png"))
        self.dirt_7 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_07.png"))
        self.dirt_8 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_08.png"))
        self.dirt_9 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_09.png"))
        self.dirt_10 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_10.png"))
        self.dirt_11 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_11.png"))
        self.dirt_12 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_12.png"))
        self.dirt_13 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_13.png"))
        self.dirt_14 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_14.png"))
        self.dirt_15 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_15.png"))
        self.dirt_16 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_16.png"))
        self.dirt_17 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_17.png"))
        self.dirt_18 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_18.png"))
        self.dirt_19 = Image.open(os.path.join(path, "Terrain/Dirt/dirt_19.png"))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.dirt_1
        if rnd == 2:
            return self.dirt_2
        if rnd == 3:
            return self.dirt_3
        if rnd == 4:
            return self.dirt_4
        if rnd == 5:
            return self.dirt_5
        if rnd == 6:
            return self.dirt_6
        if rnd == 7:
            return self.dirt_7
        if rnd == 8:
            return self.dirt_8
        if rnd == 9:
            return self.dirt_9
        if rnd == 10:
            return self.dirt_10
        if rnd == 11:
            return self.dirt_11
        if rnd == 12:
            return self.dirt_12
        if rnd == 13:
            return self.dirt_13
        if rnd == 14:
            return self.dirt_14
        if rnd == 15:
            return self.dirt_15
        if rnd == 16:
            return self.dirt_16
        if rnd == 17:
            return self.dirt_17
        if rnd == 18:
            return self.dirt_18
        if rnd == 19:
            return self.dirt_19

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 19))


class Grass:
    def __init__(self, path):
        self.grass_1 = Image.open(os.path.join(path, "Terrain/Grass/grass_01.png"))
        self.grass_2 = Image.open(os.path.join(path, "Terrain/Grass/grass_02.png"))
        self.grass_3 = Image.open(os.path.join(path, "Terrain/Grass/grass_03.png"))
        self.grass_4 = Image.open(os.path.join(path, "Terrain/Grass/grass_04.png"))
        self.grass_5 = Image.open(os.path.join(path, "Terrain/Grass/grass_05.png"))
        self.grass_6 = Image.open(os.path.join(path, "Terrain/Grass/grass_06.png"))
        self.grass_7 = Image.open(os.path.join(path, "Terrain/Grass/grass_07.png"))
        self.grass_8 = Image.open(os.path.join(path, "Terrain/Grass/grass_08.png"))
        self.grass_9 = Image.open(os.path.join(path, "Terrain/Grass/grass_09.png"))
        self.grass_10 = Image.open(os.path.join(path, "Terrain/Grass/grass_10.png"))
        self.grass_11 = Image.open(os.path.join(path, "Terrain/Grass/grass_11.png"))
        self.grass_12 = Image.open(os.path.join(path, "Terrain/Grass/grass_12.png"))
        self.grass_13 = Image.open(os.path.join(path, "Terrain/Grass/grass_13.png"))
        self.grass_14 = Image.open(os.path.join(path, "Terrain/Grass/grass_14.png"))
        self.grass_15 = Image.open(os.path.join(path, "Terrain/Grass/grass_15.png"))
        self.grass_16 = Image.open(os.path.join(path, "Terrain/Grass/grass_16.png"))
        self.grass_17 = Image.open(os.path.join(path, "Terrain/Grass/grass_17.png"))
        self.grass_18 = Image.open(os.path.join(path, "Terrain/Grass/grass_18.png"))
        self.grass_19 = Image.open(os.path.join(path, "Terrain/Grass/grass_19.png"))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.grass_1
        if rnd == 2:
            return self.grass_2
        if rnd == 3:
            return self.grass_3
        if rnd == 4:
            return self.grass_4
        if rnd == 5:
            return self.grass_5
        if rnd == 6:
            return self.grass_6
        if rnd == 7:
            return self.grass_7
        if rnd == 8:
            return self.grass_8
        if rnd == 9:
            return self.grass_9
        if rnd == 10:
            return self.grass_10
        if rnd == 11:
            return self.grass_11
        if rnd == 12:
            return self.grass_12
        if rnd == 13:
            return self.grass_13
        if rnd == 14:
            return self.grass_14
        if rnd == 15:
            return self.grass_15
        if rnd == 16:
            return self.grass_16
        if rnd == 17:
            return self.grass_17
        if rnd == 18:
            return self.grass_18
        if rnd == 19:
            return self.grass_19

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 19))


class Mars:
    def __init__(self, path):
        self.mars_1 = Image.open(os.path.join(path, "Terrain/Mars/mars_01.png"))
        self.mars_2 = Image.open(os.path.join(path, "Terrain/Mars/mars_02.png"))
        self.mars_3 = Image.open(os.path.join(path, "Terrain/Mars/mars_03.png"))
        self.mars_4 = Image.open(os.path.join(path, "Terrain/Mars/mars_04.png"))
        self.mars_5 = Image.open(os.path.join(path, "Terrain/Mars/mars_05.png"))
        self.mars_6 = Image.open(os.path.join(path, "Terrain/Mars/mars_06.png"))
        self.mars_7 = Image.open(os.path.join(path, "Terrain/Mars/mars_07.png"))
        self.mars_8 = Image.open(os.path.join(path, "Terrain/Mars/mars_08.png"))
        self.mars_9 = Image.open(os.path.join(path, "Terrain/Mars/mars_09.png"))
        self.mars_10 = Image.open(os.path.join(path, "Terrain/Mars/mars_10.png"))
        self.mars_11 = Image.open(os.path.join(path, "Terrain/Mars/mars_11.png"))
        self.mars_12 = Image.open(os.path.join(path, "Terrain/Mars/mars_12.png"))
        self.mars_13 = Image.open(os.path.join(path, "Terrain/Mars/mars_13.png"))
        self.mars_14 = Image.open(os.path.join(path, "Terrain/Mars/mars_14.png"))
        self.mars_15 = Image.open(os.path.join(path, "Terrain/Mars/mars_15.png"))
        self.mars_16 = Image.open(os.path.join(path, "Terrain/Mars/mars_16.png"))
        self.mars_17 = Image.open(os.path.join(path, "Terrain/Mars/mars_17.png"))
        self.mars_18 = Image.open(os.path.join(path, "Terrain/Mars/mars_18.png"))
        self.mars_19 = Image.open(os.path.join(path, "Terrain/Mars/mars_19.png"))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.mars_1
        if rnd == 2:
            return self.mars_2
        if rnd == 3:
            return self.mars_3
        if rnd == 4:
            return self.mars_4
        if rnd == 5:
            return self.mars_5
        if rnd == 6:
            return self.mars_6
        if rnd == 7:
            return self.mars_7
        if rnd == 8:
            return self.mars_8
        if rnd == 9:
            return self.mars_9
        if rnd == 10:
            return self.mars_10
        if rnd == 11:
            return self.mars_11
        if rnd == 12:
            return self.mars_12
        if rnd == 13:
            return self.mars_13
        if rnd == 14:
            return self.mars_14
        if rnd == 15:
            return self.mars_15
        if rnd == 16:
            return self.mars_16
        if rnd == 17:
            return self.mars_17
        if rnd == 18:
            return self.mars_18
        if rnd == 19:
            return self.mars_19

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 19))


class Sand:
    def __init__(self, path):
        self.sand_1 = Image.open(os.path.join(path, "Terrain/Sand/sand_01.png"))
        self.sand_2 = Image.open(os.path.join(path, "Terrain/Sand/sand_02.png"))
        self.sand_3 = Image.open(os.path.join(path, "Terrain/Sand/sand_03.png"))
        self.sand_4 = Image.open(os.path.join(path, "Terrain/Sand/sand_04.png"))
        self.sand_5 = Image.open(os.path.join(path, "Terrain/Sand/sand_05.png"))
        self.sand_6 = Image.open(os.path.join(path, "Terrain/Sand/sand_06.png"))
        self.sand_7 = Image.open(os.path.join(path, "Terrain/Sand/sand_07.png"))
        self.sand_8 = Image.open(os.path.join(path, "Terrain/Sand/sand_08.png"))
        self.sand_9 = Image.open(os.path.join(path, "Terrain/Sand/sand_09.png"))
        self.sand_10 = Image.open(os.path.join(path, "Terrain/Sand/sand_10.png"))
        self.sand_11 = Image.open(os.path.join(path, "Terrain/Sand/sand_11.png"))
        self.sand_12 = Image.open(os.path.join(path, "Terrain/Sand/sand_12.png"))
        self.sand_13 = Image.open(os.path.join(path, "Terrain/Sand/sand_13.png"))
        self.sand_14 = Image.open(os.path.join(path, "Terrain/Sand/sand_14.png"))
        self.sand_15 = Image.open(os.path.join(path, "Terrain/Sand/sand_15.png"))
        self.sand_16 = Image.open(os.path.join(path, "Terrain/Sand/sand_16.png"))
        self.sand_17 = Image.open(os.path.join(path, "Terrain/Sand/sand_17.png"))
        self.sand_18 = Image.open(os.path.join(path, "Terrain/Sand/sand_18.png"))
        self.sand_19 = Image.open(os.path.join(path, "Terrain/Sand/sand_19.png"))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.sand_1
        if rnd == 2:
            return self.sand_2
        if rnd == 3:
            return self.sand_3
        if rnd == 4:
            return self.sand_4
        if rnd == 5:
            return self.sand_5
        if rnd == 6:
            return self.sand_6
        if rnd == 7:
            return self.sand_7
        if rnd == 8:
            return self.sand_8
        if rnd == 9:
            return self.sand_9
        if rnd == 10:
            return self.sand_10
        if rnd == 11:
            return self.sand_11
        if rnd == 12:
            return self.sand_12
        if rnd == 13:
            return self.sand_13
        if rnd == 14:
            return self.sand_14
        if rnd == 15:
            return self.sand_15
        if rnd == 16:
            return self.sand_16
        if rnd == 17:
            return self.sand_17
        if rnd == 18:
            return self.sand_18
        if rnd == 19:
            return self.sand_19

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 19))


class Stone:
    def __init__(self, path):
        self.stone_1 = Image.open(os.path.join(path, "Terrain/Stone/stone_01.png"))
        self.stone_2 = Image.open(os.path.join(path, "Terrain/Stone/stone_02.png"))
        self.stone_3 = Image.open(os.path.join(path, "Terrain/Stone/stone_03.png"))
        self.stone_4 = Image.open(os.path.join(path, "Terrain/Stone/stone_04.png"))
        self.stone_5 = Image.open(os.path.join(path, "Terrain/Stone/stone_05.png"))
        self.stone_6 = Image.open(os.path.join(path, "Terrain/Stone/stone_06.png"))
        self.stone_7 = Image.open(os.path.join(path, "Terrain/Stone/stone_07.png"))
        self.stone_8 = Image.open(os.path.join(path, "Terrain/Stone/stone_08.png"))
        self.stone_9 = Image.open(os.path.join(path, "Terrain/Stone/stone_09.png"))
        self.stone_10 = Image.open(os.path.join(path, "Terrain/Stone/stone_10.png"))
        self.stone_11 = Image.open(os.path.join(path, "Terrain/Stone/stone_11.png"))
        self.stone_12 = Image.open(os.path.join(path, "Terrain/Stone/stone_12.png"))
        self.stone_13 = Image.open(os.path.join(path, "Terrain/Stone/stone_13.png"))
        self.stone_14 = Image.open(os.path.join(path, "Terrain/Stone/stone_14.png"))
        self.stone_15 = Image.open(os.path.join(path, "Terrain/Stone/stone_15.png"))
        self.stone_16 = Image.open(os.path.join(path, "Terrain/Stone/stone_16.png"))
        self.stone_17 = Image.open(os.path.join(path, "Terrain/Stone/stone_17.png"))
        self.stone_18 = Image.open(os.path.join(path, "Terrain/Stone/stone_18.png"))
        self.stone_19 = Image.open(os.path.join(path, "Terrain/Stone/stone_19.png"))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.stone_1
        if rnd == 2:
            return self.stone_2
        if rnd == 3:
            return self.stone_3
        if rnd == 4:
            return self.stone_4
        if rnd == 5:
            return self.stone_5
        if rnd == 6:
            return self.stone_6
        if rnd == 7:
            return self.stone_7
        if rnd == 8:
            return self.stone_8
        if rnd == 9:
            return self.stone_9
        if rnd == 10:
            return self.stone_10
        if rnd == 11:
            return self.stone_11
        if rnd == 12:
            return self.stone_12
        if rnd == 13:
            return self.stone_13
        if rnd == 14:
            return self.stone_14
        if rnd == 15:
            return self.stone_15
        if rnd == 16:
            return self.stone_16
        if rnd == 17:
            return self.stone_17
        if rnd == 18:
            return self.stone_18
        if rnd == 19:
            return self.stone_19

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 19))


class Western:
    def __init__(self, path):
        self.bank = Image.open(os.path.join(path, "Western/western_bank.png"))
        self.general = Image.open(os.path.join(path, "Western/western_general.png"))
        self.indians = Image.open(os.path.join(path, "Western/western_indians.png"))
        self.saloon = Image.open(os.path.join(path, "Western/western_saloon.png"))
        self.sheriff = Image.open(os.path.join(path, "Western/western_sheriff.png"))
        self.station = Image.open(os.path.join(path, "Western/western_station.png"))
        self.watertower = Image.open(os.path.join(path, "Western/western_watertower.png"))

    def num_to_tile(self, rnd):
        if rnd == 1:
            return self.bank
        if rnd == 2:
            return self.general
        if rnd == 3:
            return self.indians
        if rnd == 4:
            return self.saloon
        if rnd == 5:
            return self.sheriff
        if rnd == 6:
            return self.station
        if rnd == 7:
            return self.watertower

    def get_random_tile(self):
        return self.num_to_tile(random.randint(1, 7))
