from app.json_init import decode_init
from app.json_step import decode_step


class GameData:
    def __init__(self, json):
        if 'init' in json:
            self.init = decode_init(json["init"])
        else:
            raise ValueError("json not correct")
        self.steps = []
        if "steps" in json:
            for step in json["steps"]:
                self.steps.append(decode_step(step))
