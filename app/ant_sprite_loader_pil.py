import os
from PIL import Image
import pygame


class AntSprites:
    def __init__(self, path):
        path = os.path.join(path, "Ants")
        self.normalleft = Image.open(os.path.join(path, "ant_normal_left.png"))
        self.normalright = Image.open(os.path.join(path, "ant_normal_right.png"))
        self.playerright = Image.open(os.path.join(path, "ant_player_right.png"))
        self.playerleft = Image.open(os.path.join(path, "ant_player_left.png"))
        self.scout = Image.open(os.path.join(path, "ant_scout.png"))
        self.soldier = Image.open(os.path.join(path, "ant_soldier.png"))
        self.queen = Image.open(os.path.join(path, "ant_queen.png"))
