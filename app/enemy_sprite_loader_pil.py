import os

import pygame
from PIL import Image

class EnemySprites:
    def __init__(self, path):
        path = os.path.join(path, "Enemies")
        self.antlion = Image.open(os.path.join(path, "antlion.png"))
