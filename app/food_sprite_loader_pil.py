import os
from PIL import Image
import pygame


class FoodSprites:
    def __init__(self, path):
        path = os.path.join(path, "Food")
        self.red_mushroom = Image.open(os.path.join(path, "red_mushroom.png"))
