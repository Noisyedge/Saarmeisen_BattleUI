def decode_stand(stand):
    if 'swarm_id' in stand and 'score' in stand and 'ants' in stand:
        return Standing(stand['swarm_id'], stand['score'], stand['ants'])
    raise ValueError("Wrong Json")

class Standing:
    def __init__(self, swarm_id, score, ants):
        self.swarm_id = swarm_id
        self.score = score
        self.ants = ants
