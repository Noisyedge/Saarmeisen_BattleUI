from app.json_field import decode_field
from app.json_standing import decode_stand


def decode_step(step):
    if "standings" in step and "fields" in step:
        return Step(step["standings"], step["fields"])
    raise ValueError("Wrong Json")







class Step:
    def __init__(self, standings, fields):
        self.standings = []
        for stand in standings:
            self.standings.append(decode_stand(stand))
        self.fields = []
        for field in fields:
            self.fields.append(decode_field(field))


