from app.json_ant import decode_ant


def decode_field(field):
    if 'x' in field and 'y' in field and 'type' in field:
        if 'food' in field and 'ant' in field and "markers" in field:
            return Field(field["x"], field["y"], field["type"], field["markers"], field["food"], field["ant"])
        elif 'food' in field and "markers" in field:
            return Field(field["x"], field["y"], field["type"], field["markers"], field["food"])
        elif 'ant' in field and "markers" in field:
            return Field(field["x"], field["y"], field["type"], field["markers"], ant=field["ant"])
        elif "markers" in field:
            return Field(field["x"], field["y"], field["type"], field["markers"], )
        else:
            return Field(field["x"], field["y"], field["type"])
    raise ValueError("wrong field")


class Field:
    def __init__(self, x, y, field_type, markers=None, food=None, ant=None):
        self.x = x
        self.y = y
        self.markers = markers
        self.field_type = field_type
        self.food = food
        if ant is None:
            self.ant = None
        else:
            self.ant = decode_ant(ant)
