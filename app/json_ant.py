def decode_ant(ant):
    if "id" in ant and "program_counter" in ant and "swarm_id" in ant and "carries_food" in ant and "direction" in ant and "rest_time" in ant and "register" in ant:
        return Ant(ant["id"], ant["program_counter"], ant["swarm_id"], ant["carries_food"], ant["direction"],
                   ant["rest_time"], ant["register"])
    else:
        raise ValueError("wrong JSON")


class Ant:
    def __init__(self, ant_id, program_counter, swarm_id, carries_food, direction, rest_time, register):
        self.id = ant_id
        self.program_counter = program_counter
        self.swarm_id = swarm_id
        self.carries_food = carries_food
        self.direction = direction
        self.rest_time = rest_time
        self.register = register
