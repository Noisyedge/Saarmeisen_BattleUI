import os
import sys

from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


if sys.platform == "win32":
    req = ["pygame", "Pillow"]
else:
    req = ["pygame", "Pillow-SIMD"]

setup(
    name="sopra2018_battle_ui",
    version="1.1.2",
    packages=find_packages(),
    url="https://gitlab.com/Noisyedge/Saarmeisen_BattleUI",
    install_requires=req,
    long_description=read("README.txt"),
    author="Joschua Loth",
    author_email="s8joloth@stud.uni-saarland.de",
    include_package_data=True,
    license="LICENSE.txt",
    description="GUI for the sopra 2018 saants game",

)
